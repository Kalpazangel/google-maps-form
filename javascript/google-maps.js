var googleMaps = (function() {
    geocoder = new google.maps.Geocoder();
    var manualLocation = false;
    var lastMarker = null;
    $('#manualLocation').click(function() {
        initialize();
        manualLocation = true;
    });
    $('#submit').click(function() {
        manualLocation = false;
    });

    function placeMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
        });

        return marker;
    }

    function autocomplete(selector) {
        return new google.maps.places.Autocomplete(
            selector, {
                types: ['geocode']
            });
    }

    function initialize() {
        var myLatLng = {
            lat: 42.663277,
            lng: 23.365286
        };

        var mapProp = {
            center: myLatLng,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

        google.maps.event.addListener(map, 'click', function(event) {
            if (manualLocation) {
                if (lastMarker !== null) {
                    lastMarker.setMap(null);
                }
                geocoder.geocode({
                    'location': event.latLng
                }, function(results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            $('#address-input').val(results[0].formatted_address);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
                lastMarker = placeMarker(event.latLng);
            }
        });
    }

    function address() {
        var address = $('#address-input').val();
        return geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }

    return {
        autocomplete: autocomplete,
        initialize: initialize,
        address: address
    };
}());
