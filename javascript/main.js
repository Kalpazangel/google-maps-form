(function() {
    var people = [];
    for (var key in localStorage) {
        if (!isNaN(key * 1)) {
            people.push(JSON.parse(localStorage.getItem(key)));
        }
    }
    googleMaps.autocomplete(document.getElementById('address-input'));

    var app = angular.module('peopleInfo', []);

    app.controller('PeopleController', ['$scope',
        function PeopleController($scope) {
            $scope.people = people;
        }
    ]);

    app.controller('PersonController', ['$scope',
        function PersonController($scope) {
            $scope.person = {};
            $scope.addPerson = function(person) {
                localStorage.setItem(localStorage.length + 1, JSON.stringify(person));
                people.push(person);
                $scope.person = {};
            };
            $scope.setMapLocation = function(){
              googleMaps.initialize();
              googleMaps.address();
              //$('#address-input').removeClass('ng-dirty');
            };
        }
    ]);

    app.directive('formAutofillFix', function() {
      return function(scope, elem, attrs) {
        elem.prop('method', 'POST');

        if(attrs.ngSubmit) {
          setTimeout(function() {
            elem.unbind('submit').submit(function(e) {
              e.preventDefault();
              elem.find('input, textarea, select').trigger('input').trigger('change').trigger('keydown');
              scope.$apply(attrs.ngSubmit);
            });
          }, 0);
        }
      };
    });
}());
